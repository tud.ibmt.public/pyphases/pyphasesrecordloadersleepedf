from unittest import TestCase

from pyPhases import Project, pdict

from pyPhasesRecordloaderSleepEDF.Plugin import Plugin
from pyPhasesRecordloader import RecordLoader


class TestPlugin(TestCase):
    def setUp(self):
        self.options = {}
        self.project = Project()
        self.project.config = pdict(
            {
                "useLoader": "sleepedf",
            }
        )
        # self.plugin = Plugin(self.project, self.options)
        self.project.addPlugin("pyPhasesRecordloaderSleepEDF")
        self.plugin = self.project.plugins[-1]

    def test_initPlugin(self):

        self.plugin.initPlugin()

        self.assertIn("RecordLoaderSleepEDF", RecordLoader.recordLoaders)
        self.assertIn("sleepedf", self.project.config["loader"])
        self.assertEqual(self.project.config["useLoader"], "sleepedf")
        self.assertEqual(self.project.config["loader"]["sleepedf"]["dataBase"], "pyPhasesRecordloaderSleepEDF")
