# PhysioNet SleepEdfX - Plugin

[Dataset](https://physionet.org/content/sleep-edfx/1.0.0/)

pyPhases plugin that adds a Recordloader to your Project. The [RecordLoader](https://gitlab.com/tud.ibmt.public/pyphases/pyphasesrecordloader) plugin is required.
